
In this section, we first use the out-of-GPU-memory GEMM from Section~\ref{sec:gemm}
for implementing the RSVD algorithm.
%
The main data access cost of the algorithm is for moving
the matrix $\mathbf{A}$ from the CPU to the GPUs for GEMM.
%
Hence,
we specialize the GEMM implementation for RSVD to reduce the data access to $\mathbf{A}$.
%
In \Tref{tb:flops},
we summarize the computation and communication costs of different implementations 
that we study in this section.
%
The difference in the implementations is only for the power iteration, and hence 
to clarify the differences, we only show the costs for the power iteration. 
%All the other part including QR and SVD has the same $\#flops$ and $\#word$. 
%The actual cost can be calculated as pass number multiply matrix size. All the following experiments will be performed in double complex as default.

% =============================================================== %
\begin{table}[tbp]
	\small
	\centering
	\begin{tabular}{l|l|l} %% 6 column "|" means vertical line
		                          & \#flops                   & \#pass   \\
		\hline%% \\ take to the next column 
		Na\"{\i}ve                & $2(2q+1)mn\ell$           & $2q+1$ \\
		FIFO                      & $2(2q+1)mn\ell$           & $<2q+1$ \\ 
		Single-pass per iteration & $2(2q+1)mn\ell$           & $<q+1$ \\
		Explicit Gram             & $2(mn^2+qn^2\ell+mn\ell)$ & $\approx1.\times <2$  \\
	\end{tabular}
	\captionof{table}{\label{tb:flops}
                          Computation and communication costs of different implementations:
                          \#flops is the number of required flops in double precision while
                          \#pass is the number of times $\mathbf{A}$ is accessed.
                          For complex arithmetic, the \#flops should be multiplied by $4$.} 
	
\end{table}
% =============================================================== %


% ----------------------------------------------------------------- %
\subsection{Na\"{\i}ve Implementation}

% =============================================================== %
\begin{spacing}{0.8}
	\begin{algorithm}[t]
		\footnotesize
		\caption{Cholesky QR Factorization: \footnotesize
                         $\mathbf{R} := chol(\mathbf{B})$ returns the Cholesky factor of $\mathbf{B}$}
		\label{alg:cholqr}
		\SetKwInOut{Input}{Input}
		\SetKwInOut{Output}{Output}
		%\Indm
		\Input{$\mathbf{\widehat{P}}$ distributed in 1D block cyclic among GPUs, 
                       $\mathbf{\widehat{P}}_{(J,:)}$ is the $J$-th block row, and $s$ is the number of block rows.}
		\Output{Orthonormal $\mathbf{P}$ and upper-triangular $\mathbf{R}$ such that $\mathbf{P}\mathbf{R} = \widehat{\mathbf{P}}$ }
                $\mathbf{B} := 0$ \tcp*{On each GPU} 

		\For {$j = 0, 1, \cdots, s-1$}{
			$\mathbf{B}  \mathrel{+}= \widehat{\mathbf{P}}_{(J,:)}^{\top} \widehat{\mathbf{P}}_{(J,:)}$
                        \tcp*{On corresponding GPU} 
		}
		$\mathbf{R} := chol(\mathbf{B})$ \tcp*{accumulated and factored on CPU} 
		\For {$j = 0, 1, \cdots, s-1$}{
			$\mathbf{P}_{(J,:)}  := \widehat{\mathbf{P}}_{(J,:)}\mathbf{R}^{-1}$
                        \tcp*{On corresponding GPU} 
		}
	\end{algorithm}
\end{spacing}
% =============================================================== %

We use the out-of-GPU-memory GEMM (either our implementation or cuBLAS-XT) from Section~\ref{sec:gemm}
for our first ``Na\"{\i}ve'' implementation of the RSVD algorithm shown in Algorithm~\ref{alg:rsvd}.
Hence, the matrix $\mathbf{A}$ is moved to the GPUs in the 1D block row cyclic fashion.
%
Since our focus is on the tall-skinny $\mathbf{A}$ (i.e., $\ell \ll n \ll m$),
we assume that the small $n \times \ell$ projection matrix $\mathbf{Q}$ 
fits in a single GPU memory, and hence is duplicated on all the GPUs.
We evenly distribute the larger $m \times \ell$ matrix $\mathbf{P}$ among the GPUs
in the same 1D block row fashion as the matrix $\mathbf{A}$.
%
The QR factorization of~$\mathbf{P}$, distributed among the GPUs, 
is computed by Cholesky QR~\cite{Stath:2002} (shown in \fref{alg:cholqr})
that only transfers the small $\ell \times \ell$ matrix $\mathbf{B}$ and $\mathbf{R}$
between the CPU and GPUs.
Each GPU redundantly computes the QR  factorization of $\mathbf{Q}$ using the Household QR implemented by MAGMA.
%The calculation of Gram matrix $\mathbf{B}$ is ``fused'' with the calculation of $\mathbf{P}_{(J,:)}$ 
%(see Line $9$$\sim$$12$ in Algorithm~\ref{alg:single}). 
%Upper triangular matrix $\mathbf{\widetilde{B}}$ is computed by Cholesky QR~\cite{Stath:2002} on the CPU. 

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/breakdown.pdf}
	\caption{Time breakdown on one GPU with $\ell =n/10$ and $p=4$. 
        GEMM includes the time needed to transfer $\mathbf{A}$ to GPU and perform GEMM operations. 
        Misc. is composed of initialization, copying the output matrices to CPUs, 
        and random matrix generation.}
	\label{fig:breakdown}
\end{figure}
% =============================================================== %

\fref{fig:breakdown} shows the time breakdown of RSVD using our implementation of GEMM on one GPU. 
In \fref{fig:breakdown}(a), with the increasing number of rows,
RSVD spent more time in GEMMs, spending up to $95\%$ of the total time in GEMM. 
In \fref{fig:breakdown}(b) with the increasing number of columns, 
the percentage time spent for GEMM may decrease, but
RSVD still spent about $75\%$ of the total time in GEMM.

%For cuBLAS-XT implementation, the tile size is set to be big enough so as to perform 1D partition on the data. Matrix $\mathbf{P}$ and $\mathbf{U}$ are allocated as GPU memory which is faster than set to CPU memory in our experiments.

\fref{fig:naive}(a) compares the performance of RSVD using our GEMM implementation with that using cuBLAS-XT
on one GPU.
They performed similarly, and both outperformed the CPU implementation by a large margin
because the peak performance of CPUs is only one-tenth of a single V100 GPU. 
%We exclude CPU results in the following experiments. 
\fref{fig:naive}(b) shows the results on two GPUs.
Using our GEMM,
RSVD scaled well achieving the speedups of up to $1.96\times$ using two GPUs compared to one GPU.
This is
because RSVD time was dominated by GEMM which can be parallelized well using the GPUs 
(that are connected to the CPU through separate PCIe).
With cuBLAS-XT, the performance on two GPUs was lower than that on one GPU 
due to the excessive data transfer between the CPU and GPUs.
%The comparison result indicates that it is still necessary manually partition data and assign tasks in order to scale efficiently on multi-GPU.
For the remaining of this section,
we improve the performance by specializing our GEMM implementation
to reduce the data access.

%Next, we will discuss ``customized'' optimization for RSVD on GPUs.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/naive.pdf}
	\caption{Performance comparison of CPU, cuBLAS-XT and ``Na\"{\i}ve" implementations for RSVD. 
        ($n = 5000,\ \ell=500,\ q=4$). The ``Na\"{\i}ve" and cuBLAS-XT are almost overlapped in (a).}
	\label{fig:naive}
\end{figure}
% =============================================================== %



% ----------------------------------------------------------------- %
\subsection{Power Iteration with FIFO Buffer}

% =============================================================== %
\begin{spacing}{0.8}
	\begin{algorithm}[t]
		\footnotesize
		\caption{RSVD with FIFO buffer}
		\label{alg:fifo}
		\SetKwInOut{Input}{Input}
		\SetKwInOut{Output}{Output}
		%\Indm
		\Input{ matrix $\mathbf{A} \in \mathbb{Z}^{m \times n}$, target rank $k$, 
                        oversampling parameter $o$, and power iteration exponent $q$}
		\Output{  $\mathbf{U}$, $\mathbf{\Sigma}$, and $\mathbf{V}$ }
		% \Indp\Indpp
		\BlankLine
		$s := \lceil m/b \rceil$ \tcp*{$b$ is block size}

                $\ell := k+o;$

		Generate $\mathbf{Q} \sim \mathcal{N}\left(0,1\right)^{n \times \ell}$
		
		\For{$i = 0, 1, \cdots, q-1$}{
			\For {$j = 0, 1, \cdots, s-1$}{
				$\mathbf{P}_{(J,:)}  := \mathbf{A}_{(J,:)}\mathbf{Q};$
			}
				
			\For {$j = s-1, s-2, \cdots, 0$}{
				$\mathbf{Q} \mathrel{+}= \mathbf{A}_{(J,:)}^{\top}\mathbf{P}_{(J,:)}$	\tcp*{reuse 1D block}
			}
			$[\mathbf{Q},\ \sim ] := qr(\mathbf{Q});$
		}
		\For {$j = 0, 1, \cdots, s-1$}{
			$\mathbf{P}_{(J,:)} := \mathbf{A}_{(J,:)}\mathbf{Q};$
			
			$\mathbf{B}  \mathrel{+}= \mathbf{P}_{(J,:)}^{\top}\mathbf{P}_{(J,:)};$		
		}
	
		$\mathbf{R} := chol(\mathbf{B})$ \tcp*{accumulated and factored on CPU} 
		
		$[\mathbf{\widetilde{U}},\ \mathbf{\widetilde{\Sigma}},\ \mathbf{\widetilde{V}}] := svd(\mathbf{R})$ \tcp*{factored on CPU}
		
		$\mathbf{U} := \mathbf{P}\mathbf{R}^{-1} \mathbf{\widetilde{U}}\left(:, 1:k\right);$
		$\mathbf{V} := \mathbf{Q}\mathbf{\widetilde{V}}\left(:,1:k\right);$
		$ \mathbf{\Sigma} := \mathbf{\widetilde{\Sigma}}(1:k,1:k);$
	\end{algorithm}
\end{spacing}
% =============================================================== %

The Na\"{\i}ve implementation accesses the matrix $\mathbf{A}$ twice per iteration 
(first for performing GEMM with $\mathbf{A}$, and then for performing GEMM with $\mathbf{A}$).
Our next implementation is identical to the Na\"{\i}ve implementation except that during the power iteration,
for GEMM with $\mathbf{A}^{\top}$, the matrix is access from the bottom blocks
that are still in the GPU memory after GEMM with $\mathbf{A}$.
For GEMM with $\mathbf{A}$ at the next iteration, 
the matrix is accessed from the top blocks that are still in the GPU memory.
Algorithm~\ref{alg:fifo} illustrates this implementation with an FIFO buffer scheme.
%In the iteration loop of Algorithm \ref{alg:single}, the block index $j$ will go back and forth so as to reuse block in the buffer between each iteration. 
%The calculation of Gram matrix $\mathbf{B}$ is ``fused'' with the calculation of $\mathbf{P}_{(J,:)}$ 
%(see Line $9$$\sim$$12$ in Algorithm~\ref{alg:single}). 

Besides moving the matrix $\mathbf{A}$ from the CPU to the GPU,
all of our RSVD implementations transfer only the two small compressed matrices
$\widetilde{\mathbf{Q}}$ and $\mathbf{B}$
with the dimensions $n \times \ell$ and $\ell \times \ell$, respectively.

% ----------------------------------------------------------------- %
\subsection{Power Iteration with Single-pass per Iteration}

The previous implementation specialized
the GPU memory management for RSVD, but
it did not change the RSVD algorithm.
To further improve the data reuse,
our next implementation
takes advantage of the fact that
the orthogonalization of $\mathbf{P}$ may not be needed.
These orthogonalizations are used to ensure that
the different columns of $\mathbf{P}$ or $\mathbf{Q}$ do not
converge to the same dominant singular vector.
However, only the orgonalization of $\mathbf{Q}$ may be needed
for many matrices from the data science (see Section~\ref{sec:accuracy}).

In the new implementation,
for each block row $\mathbf{A}_{(J,:)}$ brought to the GPU,
we perform GEMM with both $\mathbf{A}_{(J,:)}$ and $\mathbf{A}_{(J,:)}^{\top}$
before the next block is brought to the GPU.
Algorithm~\ref{alg:single} shows the implementation where
the block $\mathbf{A}_{(J,:)}$ is reused on Lines~4 and 5
before the next block is brought to the GPU.
Hence, the matrix $\mathbf{A}$ is now accessed only once 
in every iteration. %for performing GEMM with both $\mathbf{A}$ and $\mathbf{A}^{\top}$.
The overhead of this ``single-pass'' implementation
is the extra buffer $\widetilde{\mathbf{Q}}$
for temporarily storing $\mathbf{Q}$. 
The extra buffer is needed because $\mathbf{Q}$ cannot be overwritten on Line 4
because it is used on Line 3.


% =============================================================== %
\begin{spacing}{0.8}
	\begin{algorithm}[t]
		\footnotesize
		\caption{Power Iteration with single-pass per iteration}
		\label{alg:single}
		%\SetKwInOut{Input}{Input}
		%\SetKwInOut{Output}{Output}
		%\Indm
		%\Input{ matrix $\mathbf{A} \in \mathbb{Z}^{m \times n}$, target rank $k$, oversampling parameter $o$, and power iteration exponent $q$, $J$ denotes index set $jm_b:(j+1)m_b-1$}
		%\Output{  $\mathbf{U}$, $\mathbf{\Sigma}$, and $\mathbf{V}$ }
		% \Indp\Indpp
		\BlankLine
		%$m_b = \lceil m/s \rceil;\ \ l = k+o; \ \ \mathbf{P} = \mathbf{0}_{m \times l}$;
		%Generate $\mathbf{Q} \sim \mathcal{N}\left(0,1\right)^{n \times l}$
		\For{$i = 0, 1, \cdots, q-1$}{
			\For {$j = 0, 1, \cdots, s-1$}{
				$\mathbf{P}_{(J,:)}  := \mathbf{A}_{(J,:)}\mathbf{Q};$
				
				$\mathbf{\widetilde{Q}} \mathrel{+}= \mathbf{A}_{(J,:)}^{\top}\mathbf{P}_{(J,:)}$	\tcp*{reuse 1D block}

			}
			$[\mathbf{Q},\ \sim ] := qr(\mathbf{\widetilde{Q}} );$
		}
		%\For {$j \gets 0$ {\rm \bf to} $s-1$}{
		%	$\mathbf{P}_{(J,:)} = \mathbf{A}_{(J,:)}\mathbf{Q};$
		%	
		%	$\mathbf{B}  \mathrel{+}= \mathbf{P}_{(J,:)}^{\top}\mathbf{P}_{(J,:)};$		
		%}
	
		%$\mathbf{\widetilde{B}} = cholesky(\mathbf{B} )$ \tcp*{Cholesky QR on CPU} 
		
		%$[\mathbf{\widetilde{U}},\ \mathbf{\widetilde{\Sigma}},\ \mathbf{\widetilde{V}}] = svd(\mathbf{\widetilde{B}});$
		
		%$\mathbf{U} = \mathbf{P}\mathbf{\widetilde{B}}^{-1} \mathbf{\widetilde{U}}\left(:, 1:k\right);$
		%$\mathbf{V} = \mathbf{Q}\mathbf{\widetilde{V}}\left(:,1:k\right);$
		%$ \mathbf{\Sigma} = \mathbf{\widetilde{\Sigma}}(1:k,1:k);$
	\end{algorithm}
\end{spacing}
% =============================================================== %


% ----------------------------------------------------------------- %
\subsection{Power Iteration with Explicit Gram matrix}

Our final implementation explicitly computes the Gram matrix, $\mathbf{G} := \mathbf{A}^{\top} \mathbf{A}$, and
then perform the power iterations on~$\mathbf{G}$.
Hence, the algorithm
accesses the matrix $\mathbf{A}$ only once to compute $\mathbf{G}$
for the entire power iterations.
Though this reduces the data access, it requires the extra storage and computation to form the Gram matrix
(i.e., $\mathcal{O}(n^2)$ storage and $\mathcal{O}(mn^2)$ flops). 
Hence, unless the matrix requires a significant number of iterations ($q > \frac{n}{2\ell}$),
this new implementation may need much more flops than our previous implementations.
However, when the data access dominates the RSVD time,
then explicitly forming $\mathbf{G}$ may reduce the total time.
%
%Some data may present a slow singular decay pattern which requires more iterations to converge. 
%As the $q$ increases, more passes to $\mathbf{A}$ will incur tremendous communication cost. 
%We propose a new approach which forms GRAM matrix of $\mathbf{A}$ in advance. 
%Line 3 and 4 of Algorithm \ref{alg:rsvd} can be combined as $\mathbf{Q} = \mathbf{A}^{\top}\mathbf{A}\mathbf{Q}$. 
%This is repeated $q$ times and has $4qmnl$ flops. 
%We form the gram matrix of $\mathbf{A}$ as $\mathbf{G} = \mathbf{A}^{*}\mathbf{A}$ and use $\mathbf{G}$ in power iteration. 
The new implementation is shown in Algorithm \ref{alg:gram}. It is algorithmically equivalent to Algorithm \ref{alg:single}.
%It is mathematically equivalent to Algorithm \ref{alg:rsvd}, which only changes the GEMM operation sequence. 
%For theoretical analysis on power iteration, please refer to ~\cite{martinsson2010normalized, halko2011finding}.

% =============================================================== %
\begin{algorithm}[t]
	\small
	\caption{Power iteration with explicit Gram matrix}
	\label{alg:gram}
	%\SetKwInOut{Input}{Input}
	%\SetKwInOut{Output}{Output}
	%%	\Indm
	%\Input{ matrix $\mathbf{A} \in \mathbb{Z}^{m \times n}$, target rank $k$, oversampling parameter $o$, and power iteration exponent $q$}
	%\Output{  $\mathbf{U}$, $\mathbf{\Sigma}$, and $\mathbf{V}$ }
	
	%%	\Indp\Indpp
	\BlankLine
	%Generate a random matrix $\mathbf{Q} \sim \mathcal{N}\left(0,1\right)^{n \times l}$,
	%where $l = k + o$.
	
	$\mathbf{G} = \mathbf{A}^{\top}\mathbf{A}$ \tcp*{Form Gram matrix by 1D GEMM}
	
	\For{$i = 0, 1, \cdots, q-1$}{
		$\mathbf{Q} := \mathbf{G}\mathbf{Q};$
		
		$\left[\mathbf{Q},\ \sim \right] := qr(\mathbf{Q});$
	}
	%$\mathbf{P} = \mathbf{A}\mathbf{Q};$
	
	%$\left[ \mathbf{P},\ \mathbf{B}\right] = qr\left(\mathbf{P}\right);$
	
	%$\left[\mathbf{\widetilde{U}},\ \mathbf{\widetilde{\Sigma}},\ \mathbf{\widetilde{V}}\right] = svd\left(\mathbf{B}\right);$
	
	%$\mathbf{U} = \mathbf{P} \mathbf{\widetilde{U}}\left(:, 1:k\right);$
	%$\mathbf{V} = \mathbf{Q}\mathbf{\widetilde{V}}\left(:,1:k\right);$
	%$ \mathbf{\Sigma} = \mathbf{\widetilde{\Sigma}}(1:k,1:k);$
	
\end{algorithm}
% =============================================================== %

%As the arithmetic and communication cost of sampling and power iteration are listed in \Tref{tb:flops}, we count the other part of the algorithm as $2(mnl+mlk+nkl)$ and $mn+nlq+ml+2l^2$ for arithmetic cost and communication cost respectively.


% ----------------------------------------------------------------- %
\subsection{Numerical Studies with Synthetic and Real Data}
\label{sec:accuracy}

We use synthetic and real data sets to
evaluate the numerical stability of different implementations 
of computing the truncated SVD:
(1)~LAPACK's full SVD, 
(2)~our Na\"{\i}ve implementation that orthogonalizes both $\mathbf{P}$ and $\mathbf{Q}$, and 
(3)~our implementation with the explicit Gram matrix that orthogonalizes only $\mathbf{Q}$.
We used two different singular value distribution for the synthetic data sets: geometric and exponential. 
For the geometric distribution, 
the $j$-$th$ singular value $\sigma_{j}$ was defined  as \hbox{$\sigma_{j} = \sigma_{1}\gamma^{j-1}$}
with the parameter \hbox{$\gamma = 0.99$}.
For the exponential distribution, it is defined as \hbox{$\sigma_{j} = \sigma_{1}e^{-\frac{j}{\beta}}$}
with the parameter \hbox{$\beta = 160$}.
%
The real data set comes from the FERET dataset~\cite{phillips2000feret}. 
All the $25,389$ images are resized to the resolution of $512$$\times$$768$, and
thus the input matrix size is $39,3216$$\times$$25,389$. 
%The approximation error is defined as $e =  \norm{\mathbf{A} - \hat{\mathbf{A}} }_F /\ \norm{ \mathbf{A}}_F$. 
Hence, for these data sets, the orthogonalization of $\mathbf{P}$ was not needed.

% =============================================================== %
\begin{table}[t]
	\footnotesize
	\centering
	\begin{tabular}{l|c|cc|cc} %% 6 column "|" means vertical
		& LAPACK & \multicolumn{2}{c|}{Na\"{\i}ve} & \multicolumn{2}{c}{Gram matrix}\\
		\hline%% \\ take to the next
	    & & $q=1$ & $q=4$ & $q=1$ & $q=4$  \\
		\hline%% \\ take to the next column 
		Geometric & 
		$5.204\mathrm{e}$-$1$ &
		$5.297\mathrm{e}$-$1$ & $5.204\mathrm{e}$-$1$& $5.302\mathrm{e}$-$1$& $5.204\mathrm{e}$-$1$\\ 
		Exponential & $6.622\mathrm{e}$-$1$ & $6.828\mathrm{e}$-$1$ & 
		$6.623\mathrm{e}$-$1$& $6.830\mathrm{e}$-$1$& $6.623\mathrm{e}$-$1$ \\
		FERET & $1.661\mathrm{e}$-$1$ &$1.690\mathrm{e}$-$1$ & $1.661\mathrm{e}$-$1$ & $1.690\mathrm{e}$-$1$ & $1.661\mathrm{e}$-$1$ \\

	\end{tabular}
	\caption{Approximation error $\frac{\norm{\mathbf{A} - \hat{\mathbf{A}} }_F}{\norm{ \mathbf{A}}_F}$
                 with $k=64$, $o=64$, $m = 10^5$, and $n=5000$.}
	\label{tb:err}
	
\end{table}
% =============================================================== %

The results in ~\Tref{tb:err} show that 
our two implementations give similar approximation errors. 
%Increased power iteration $q$ improves the accuracy slightly for randomized method. 
With $q=4$, our approximation errors were about the same as those of the deterministic LAPACK implementation. 


% ----------------------------------------------------------------- %
\subsection{Performance}

Finally, we study the performance of our four different implementations.
Figs.~\ref{fig:gram}(a) and (b) show that the performance of the FIFO implementation
was higher than that of the Na\"{\i}ve implementation
by about $10\%$ (by reusing the data in the GPU memory as much as possible). 
Furthermore, by reducing the number of the data passes from $2q+1$ to $q+1$, 
the single-pass-per-iteration improved the performance
by a factor of about $2\times$.
Finally,
the run time was almost halved by explicitly forming the Gram matrix.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/gram.pdf}
	\caption{RSVD performance with different implementations, and increasing number of rows in $\mathbf{A}$ ($\ell = \frac{n}{10}$ and $p=4$). }
	 %(c) and (d) are conducted in double precision instead of double complex in order to show ``wider'' matrices.
	\label{fig:gram}
\end{figure}
% =============================================================== %

\fref{fig:power} shows the run time of ``Single-pass'' and ``Gram matrix'' implementations 
with the increasing number of power iterations, $q$.
The run time of Single-pass increases linearly with~$q$ in all the setups. 
%For matrix with small column ($n=1000$ in \fref{fig:power}(a) and (b)), 
%the ``Gram'' shows its advantage that the run time almost stand still with the increasing $q$. 
%With a small iteration count and a large number of columns ($n=5000$ in \fref{fig:power}(c) and (d)), ``Single-pass'' is faster 
%due to the smaller computation costs.
With a small iteration count $q$,
when the matrix $\mathbf{A}$ is not tall-skinny enough (i.e., a smaller ratio of $m$ over $n$),
Figs~\ref{fig:power}(c) and~(d) indicate that the Gram matrix could become slower than the Single-pass
as the number of columns in $\mathbf{A}$ increases (due to increased flops count to form $\mathbf{G}$).
However, with the explicit Gram matrix,
the data access cost does not change with $q$, and
the increasing number of $q$ did not increase the overall run time. 
Hence, Gram matrix could obtain a significant performance improvement,
especially when a large number of iterations is needed.
%With the increasing $q$, the run time of ``Single'' surpasses ``Gram''.   
%This results gives a hint for choosing the right strategy in processing matrices with different sizes.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/power.pdf}
	\caption{RSVD performance with increasing number of power iterations \mbox{($\ell = \frac{n}{10}$)}.}
	\label{fig:power}
\end{figure}
% =============================================================== %
