clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 400]);

bandwidth = 0.013; apeak = 7.0; %% 13GB/s for pci-e 12Tflops for 2GPU

%%%%%%%%%%%%%%%%%(a) 1gpu time %%%%%%%%%%%%%%%%%%%%%
subplot(1, 2, 1);
one_gpu = csvread('cpu_xt_1gpu.csv');
m1= one_gpu(:, 1);
cpu1 = one_gpu(:, 4);
basic1 = one_gpu(:, 5);
xt1 = one_gpu(:, 9);

%m1, cpu1, '->', ...
plot(m1, basic1,  '-*', ...
     m1, xt1, '--x', ...
     m1, cpu1, '-o', ...
     'LineWidth', LineWide);
xlim([min(m1) max(m1)]);

h_legend = legend('Naive (GPU)','cuBLAS-XT','two CPUs',... 
       'Orientation','Vertical','Location','northwest');
   
% set(h_legend, 'FontSize', fsize);
axis square;

%set(gca,'Xscale', 'log'); set(gca,'Yscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) one GPU'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

% %%%%%%%%%%%%%%%%% (b) 2GPUs %%%%%%%%%%%%%%%%%%%%%
subplot(1, 2, 2);

two_gpu = csvread('cpu_xt_2gpu.csv');
m2= two_gpu(:, 1);
cpu2 = two_gpu(:, 4);
basic2 = two_gpu(:, 5);
xt2 = two_gpu(:, 9);

%m1, cpu1, '->', ...
plot(m2, basic2,  '-*', ...
     m2, xt2, '--x', ...
     'LineWidth', LineWide);
xlim([min(m1) max(m1)]);

h_legend = legend('Naive (GPU)','cuBLAS-XT',... 
       'Orientation','Vertical','Location','northwest');
   
% set(h_legend, 'FontSize', fsize);
axis square;
%set(gca,'Xscale', 'log');
%set(gca,'Yscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(b) tow GPUs'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

basic1./basic2
xt1./xt2

% power = csvread('power.csv');
% m = power(1:5:end, 1);
% single_p1 = power(1:5:end, 5);
% single_p2 = power(2:5:end, 5);
% single_p3 = power(3:5:end, 5);
% single_p4 = power(4:5:end, 5);
% single_p5 = power(5:5:end, 5);
% 
% gram_p1 = power(1:5:end, 6);
% gram_p2 = power(2:5:end, 6);
% gram_p3 = power(3:5:end, 6);
% gram_p4 = power(4:5:end, 6);
% gram_p5 = power(5:5:end, 6);
% 
% plot(m, single_p1, '-', ...
%      m, single_p2, ':', ...
%      m, single_p3, '-o', ...
%      m, single_p4, '--', ... 
%      m, single_p5, '-x', ...
%      m, gram_p1, '-*',...
%      m, gram_p2, '-^',...
%      m, gram_p3, '->',...
%      m, gram_p4, '-+',... 
%      m, gram_p5, '-s',...     
%     'LineWidth', LineWide);
% 
% xlim([min(m) max(m)]);
% %ylim([0 apeak]);
% set(gca, 'fontsize', fsize);
% h_legend = legend('Single p=1', 'Single p=2','Single p=3',...
%                   'Single p=4', 'Single p=5',...
%                   'Gram p=1', 'Gram p=2', 'Gram p=3',...
%                   'Gram p=4', 'Gram p=5',...
%        'Orientation','Vertical','Location','northwest');
% 
% 
% xlabel({'$m$','(b) $n = 1000,\ l=100$'}, ...
%        'Interpreter', 'latex', 'fontsize', largeFont);
% ylabel('time (s)', 'fontsize', largeFont);
% grid on; set(gca,'GridLineStyle',':');


% %%%%%%%%%%%%%%%%% (b) 1 GPU roofline model%%%%%%%%%%%%%%%%%%%%%
% m = one_gpu(:, 1);
% n = one_gpu(:, 2);
% k = one_gpu(:, 3);
% l = 2 .* k;
% subplot(2, 2, 2);
% %% (5e5* (5e5/50) * 16)/ 2^30 = 74.5GB for double complex
% %% naive power iteration flops
% q = 4;
% %% GEMM flops
% basic_flops = 8 * (2*q+1).*( m.*n.*l); % double complex
% gram_flops =  8 * (m.*(n.^2) + q * (n.^2).*l + m.*n.*l);
% 
% %% other flops
% o_flops = 8 * (m.*(l.^2)*q + m.*l.*k + n.*l.*k + l.^3);
% %% word
% basic_word = 16 * ((2*q+1) .* m .* n);
% gram_word = 16 * ( 2 * m .* n );
% o_word = 16 * (n.*l.*q + m.*l + 2*(l.^2));
% 
% basic_intense = (basic_flops + o_flops) ./ (basic_word + o_word);
% gram_intense  = (gram_flops  + o_flops) ./ (gram_word + o_word);
% basic_roof = min(basic_intense * bandwidth, apeak);
% gram_roof  = min(gram_intense * bandwidth, apeak);
% basic_total = (basic_flops + o_flops)./1e12; 
% gram_total  = (gram_flops  + o_flops)./1e12;
% 
% basic_flop = basic_total ./ basic1;
% gram_flop =  gram_total ./ gram1;
% %     
% plot(m, basic_roof, '-', ...
%      m, gram_roof, '-', ...
%      m, basic_flop, '-o', ...
%      m, gram_flop, '-^', ... 
%     'LineWidth', LineWide);
% xlim([min(m) max(m)]);
% %ylim([0 apeak]);
% 
% h_legend = legend('baisc roofline',...
%        'Gram roofline',...
%        'Basic',...
%        'Gram',...
%        'Orientation','Vertical','Location','northeast');
% 
% % set(h_legend, 'FontSize', fsize);
% axis square;
% 
% set(gca,'Xscale', 'log');
% set(gca, 'fontsize', fsize);
% %set(gca,'Xscale', 'log','Yscale','log');
% xlabel({'$m$'}, ...
%        'Interpreter', 'latex', 'fontsize', largeFont);
% ylabel('Tflop/s', 'fontsize', largeFont);
% grid on; set(gca,'GridLineStyle',':');
