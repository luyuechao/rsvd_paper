clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 800]);

%%%%%%%%%%%%%%%%%(a) 1gpu  1000 %%%%%%%%%%%%%%%%%%%%%
subplot(2, 2, 1);

power = csvread('power_1gpu_1000.csv');
m = power(1:5:end, 1);
single_p1 = power(1:5:end, 5);
single_p2 = power(2:5:end, 5);
single_p3 = power(3:5:end, 5);
single_p4 = power(4:5:end, 5);
single_p5 = power(5:5:end, 5);

gram_p1 = power(1:5:end, 6);
gram_p2 = power(2:5:end, 6);
gram_p3 = power(3:5:end, 6);
gram_p4 = power(4:5:end, 6);
gram_p5 = power(5:5:end, 6);

plot(m, single_p1, '--^', ...
     m, single_p3, '--.', ...
     m, single_p4, '--o', ...
     m, single_p5, '--x', ...
     m, gram_p1, '-*',...
     m, gram_p3, '->',... 
     m, gram_p5, '-s',...     
    'LineWidth', LineWide);

xlim([min(m) max(m)]);
%ylim([0 apeak]);
set(gca, 'fontsize', fsize);
axis square;
% h_legend = legend('Single, $q=1$', 'Single, $q=2$','Single, $q=4$','Single, $q=10$',...
%                   'Gram, $q=1$', 'Gram, $q=4$', 'Gram, $q=10$',...
%        'Orientation','Vertical','Location','northwest');
% set(h_legend,'Interpreter','latex');
xlabel({'$m$','(a) one GPU, $n=1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');


% %%%%%%%%%%%%%%%%% (b) 2 GPU 1000 %%%%%%%%%%%%%%%%%%%%%
subplot(2, 2, 2);

power = csvread('power_2gpu_1000.csv');
m = power(1:5:end, 1);
single_p1 = power(1:5:end, 5);
single_p2 = power(2:5:end, 5);
single_p3 = power(3:5:end, 5);
single_p4 = power(4:5:end, 5);
single_p5 = power(5:5:end, 5);

gram_p1 = power(1:5:end, 6);
gram_p2 = power(2:5:end, 6);
gram_p3 = power(3:5:end, 6);
gram_p4 = power(4:5:end, 6);
gram_p5 = power(5:5:end, 6);
%marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};
plot(m, single_p1, '--^', ...
     m, single_p3, '--.', ...
     m, single_p4, '--o', ...
     m, single_p5, '--x', ...
     m, gram_p1, '-*',...
     m, gram_p3, '->',... 
     m, gram_p5, '-s',...     
    'LineWidth', LineWide);

xlim([min(m) max(m)]);
%ylim([0 apeak]);
set(gca, 'fontsize', fsize);
axis square;
% h_legend = legend('Single, $q=1$', 'Single, $q=4$','Single, $q=8$','Single, $q=10$',...
%                   'Gram, $q=1$', 'Gram, $q=4$', 'Gram, $q=10$',...
%        'Orientation','Vertical','Location','northwest');
% set(h_legend,'Interpreter','latex');
xlabel({'$m$','(b) two GPUs, $n=1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

% %%%%%%%%%%%%%%%%% (c) 1 GPU 5000 %%%%%%%%%%%%%%%%%%%%%
subplot(2, 2, 3);

power = csvread('power_1gpu_5000.csv');
m = power(1:5:end, 1);
single_p1 = power(1:5:end, 5);
single_p2 = power(2:5:end, 5);
single_p3 = power(3:5:end, 5);
single_p4 = power(4:5:end, 5);
single_p5 = power(5:5:end, 5);

gram_p1 = power(1:5:end, 6);
gram_p2 = power(2:5:end, 6);
gram_p3 = power(3:5:end, 6);
gram_p4 = power(4:5:end, 6);
gram_p5 = power(5:5:end, 6);

plot(m, single_p1, '--^', ...
     m, single_p3, '--.', ...
     m, single_p4, '--o', ...
     m, single_p5, '--x', ...
     m, gram_p1, '-*',...
     m, gram_p3, '->',... 
     m, gram_p5, '-s',...     
    'LineWidth', LineWide);

xlim([min(m) max(m)]);
%ylim([0 apeak]);
set(gca, 'fontsize', fsize);
axis square;
% h_legend = legend('Single, $q=1$', 'Single, $q=2$','Single, $q=4$','Single, $q=10$',...
%                   'Gram, $q=1$', 'Gram, $q=4$', 'Gram, $q=10$',...
%        'Orientation','Vertical','Location','northwest');
% set(h_legend,'Interpreter','latex');
xlabel({'$m$','(c) one GPU, $n=5000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');



% %%%%%%%%%%%%%%%%% (d) 1 GPU 5000 %%%%%%%%%%%%%%%%%%%%%
subplot(2, 2, 4);
power = csvread('power_2gpu_5000.csv');
m = power(1:5:end, 1);
single_p1 = power(1:5:end, 5);
single_p2 = power(2:5:end, 5);
single_p3 = power(3:5:end, 5);
single_p4 = power(4:5:end, 5);
single_p5 = power(5:5:end, 5);

gram_p1 = power(1:5:end, 6);
gram_p2 = power(2:5:end, 6);
gram_p3 = power(3:5:end, 6);
gram_p4 = power(4:5:end, 6);
gram_p5 = power(5:5:end, 6);

plot(m, single_p1, '--^', ...
     m, single_p3, '--.', ...
     m, single_p4, '--o', ...
     m, single_p5, '--x', ...
     m, gram_p1, '-*',...
     m, gram_p3, '->',... 
     m, gram_p5, '-s',...     
    'LineWidth', LineWide);

xlim([min(m) max(m)]);
%ylim([0 apeak]);
set(gca, 'fontsize', fsize);
axis square;
% h_legend = legend('Single, $q=1$', 'Single, $q=4$','Single, $q=8$','Single, $q=10$',...
%                   'Gram, $q=1$', 'Gram, $q=4$', 'Gram, $q=10$',...
%        'Orientation','Vertical','Location','northwest');
% set(h_legend,'Interpreter','latex');
xlabel({'$m$','(d) two GPUs, $n=5000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

% m = one_gpu(:, 1);
% n = one_gpu(:, 2);
% k = one_gpu(:, 3);
% l = 2 .* k;
% subplot(2, 2, 2);
% %% (5e5* (5e5/50) * 16)/ 2^30 = 74.5GB for double complex
% %% naive power iteration flops
% q = 4;
% %% GEMM flops
% basic_flops = 8 * (2*q+1).*( m.*n.*l); % double complex
% gram_flops =  8 * (m.*(n.^2) + q * (n.^2).*l + m.*n.*l);
% 
% %% other flops
% o_flops = 8 * (m.*(l.^2)*q + m.*l.*k + n.*l.*k + l.^3);
% %% word
% basic_word = 16 * ((2*q+1) .* m .* n);
% gram_word = 16 * ( 2 * m .* n );
% o_word = 16 * (n.*l.*q + m.*l + 2*(l.^2));
% 
% basic_intense = (basic_flops + o_flops) ./ (basic_word + o_word);
% gram_intense  = (gram_flops  + o_flops) ./ (gram_word + o_word);
% basic_roof = min(basic_intense * bandwidth, apeak);
% gram_roof  = min(gram_intense * bandwidth, apeak);
% basic_total = (basic_flops + o_flops)./1e12; 
% gram_total  = (gram_flops  + o_flops)./1e12;
% 
% basic_flop = basic_total ./ basic1;
% gram_flop =  gram_total ./ gram1;
% %     
% plot(m, basic_roof, '-', ...
%      m, gram_roof, '-', ...
%      m, basic_flop, '-o', ...
%      m, gram_flop, '-^', ... 
%     'LineWidth', LineWide);
% xlim([min(m) max(m)]);
% %ylim([0 apeak]);
% 
% h_legend = legend('baisc roofline',...
%        'Gram roofline',...
%        'Basic',...
%        'Gram',...
%        'Orientation','Vertical','Location','northeast');
% 
% % set(h_legend, 'FontSize', fsize);
% axis square;
% 
% set(gca,'Xscale', 'log');
% set(gca, 'fontsize', fsize);
% %set(gca,'Xscale', 'log','Yscale','log');
% xlabel({'$m$'}, ...
%        'Interpreter', 'latex', 'fontsize', largeFont);
% ylabel('Tflop/s', 'fontsize', largeFont);
% grid on; set(gca,'GridLineStyle',':');
