clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 800]);

bandwidth = 0.013*2; apeak = 7.0 * 2; %% 13GB/s for pci-e 12Tflops for 2GPU
data = csvread('multi_1D_perf.csv');
blasX = csvread('BLASX_2GPU.csv');
%%%%%%%%%%%%%%%%%(a) square 1000 * 1000 %%%%%%%%%%%%%%%%%%%%%

idx1 = 1:13;
Xaxis1 = data(idx1, 2);
multi = data(idx1, 8);
XTpin  = data(idx1, 10);
XTuma  = data(idx1, 12);
XT2D = data(idx1, 14);

subplot(2, 2, 1);
m = Xaxis1; n = 1000;
op1D1 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D1 * bandwidth, apeak);

plot(Xaxis1, roofline1D, '-', ...
     Xaxis1, multi, '-o',...
     Xaxis1, XTpin, '-*',...
     Xaxis1, XTuma, '-x',...
     Xaxis1, XT2D, '-s',...
     blasX(1:12, 1), blasX(1:12, 5), '-+',...
     'LineWidth', LineWide);
xlim([min(m) max(m)]);
ylim([0 1.1* max(roofline1D)]);

% h_legend = legend('roofline',...
%        'Proposed',...
%        'cuBLAS-XT, pinned memory, 1D',...
%        'cuBLAS-XT, UMA, 1D',...
%        'cuBLAS-XT, pinned memory, 2D ',...
%        'BLASX',...
%        'Orientation','Vertical','Location','south');

% set(h_legend, 'FontSize', fsize);
axis square;

set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) $k = 1000,\ n = 1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%%%%%%%%%%% (b) square 5000*5000 %%%%%%%%%%%%%%%%%%%%%%%%
idx2 = 14:24;
Xaxis2 = data(idx2, 2);
multi = data(idx2, 8);
XTpin  = data(idx2, 10);
XTuma  = data(idx2, 12);
XT2D = data(idx2, 14);

%blasx = csvread('BLASX_5000.csv');
subplot(2,2,2);
m = Xaxis2; n = 5000;
op1D2 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D2 * bandwidth, apeak);

plot(Xaxis2, roofline1D, '-', ...
     Xaxis2, multi,'-o',...
     Xaxis2, XTpin,'-*',...
     Xaxis2, XTuma,'-x',...
     Xaxis2, XT2D, '-s',...
     blasX(13:21, 1), blasX(13:21, 5), '-+',...
     'LineWidth', LineWide);
     %blasx(:, 1), blasx(:, 5),'-+',...
xlim([min(Xaxis2) max(Xaxis2)]);


axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(b) $k=5000,\ n = 5000$'}, 'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);

grid on; set(gca,'GridLineStyle',':');


%%%%%%%%%% (c) rectangle 5000*500 %%%%%%%%%%%%%%%%%%%%%%%%
idx3 = 25:35;
Xaxis3 = data(idx3, 2);
multi = data(idx3, 8);
XTpin  = data(idx3, 10);
XTuma  = data(idx3, 12);
XT2D = data(idx3, 14);

subplot(2, 2, 3);
m = Xaxis3; k = 5000; n = 500;
op1D3 = (m .* (n * k)) ./ (4*(m.*k + n.*k + m.*n));
roofline1D = min(op1D3 * bandwidth, apeak);

plot(Xaxis3, roofline1D, '-', ...
     Xaxis3, multi,'-o',...
     Xaxis3, XTpin,'-*',...
     Xaxis3, XTuma,'-x',...
     Xaxis3, XT2D, '-s',...
     blasX(22:30, 1), blasX(22:30, 5), '-+',...
     'LineWidth', LineWide);

xlim([min(Xaxis2) max(Xaxis2)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(c) $k=5000,\ n = 500$'},'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%% (d) rectangle 1000*10000 %%%%%%%%%%%%%%%%%%%%%%%%
idx4 = 36:46;
Xaxis4 = data(idx4, 2);
multi = data(idx4, 8);
XTpin  = data(idx4, 10);
XTuma  = data(idx4, 12);
XT2D = data(idx4, 14);

subplot(2, 2, 4);
m = Xaxis4; k = 1000; n = 10000;
op1D4 = (m .* (n * k)) ./ (4*(m.*k + n.*k + m.*n));
roofline1D = min(op1D4 * bandwidth, apeak);

plot(Xaxis4, roofline1D, '-',...
     Xaxis4, multi, '-o',...
     Xaxis4, XTpin, '-*',...
     Xaxis4, XTuma, '-x',...
     Xaxis3, XT2D, '-s',...
     blasX(31:38, 1), blasX(31:38, 5), '-+',...
     'LineWidth', LineWide);
 
     %blasx(:, 1), blasx(:, 5), '-+',...
xlim([min(Xaxis4) max(Xaxis4)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(d) $k=1000,\ n = 10000$'},'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');
