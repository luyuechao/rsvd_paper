clc; clear; close all;

fsize = 14; LineWide = 2;
h1=figure; set(h1, 'position', [0, 0, 1500, 1000]);

m=[1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,...
    20000,30000,40000,50000,60000,70000,80000,90000,100000,110000];

n=[1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,...
   13000,16000,19000,22000,25000,28000,31000,34000,37000,40000];

m_length = length(m);
n_length = length(n);
%%%%%%%%%%%%%%%%%%%%%%%%%%%(a)%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2, 3, 1);
Op2D = zeros(m_length, n_length); % operational intensity
bandwidth = 0.013;  % CPU-GPU bandwidth in Tflops
apeak = 6;  % arithmetic peak
% for test

% row major data!
for i = 1:m_length
    for j = 1:n_length
        Op2D(j,i) = (m(i).*n(j))...
            ./ ((2* m(i) + n(j))*4);
    end
end

roofline1 = min(Op2D * bandwidth, apeak);

surf(m, n, roofline1);

xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);
view(2); %% 2D view
colormap(jet(256)); hcb =colorbar; 
title(hcb,'Tflop/s', 'fontsize', fsize);
shading interp;
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$','(a) word\#: mn+nk+mn '},...
       'Interpreter', 'latex', 'fontsize', 20);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', 20);
zlabel('Tflop/s','fontsize', fsize);
axis square;
title('Roofline model (1): best case', 'fontsize', fsize);

%%%%%%%%%%%%%%%%%%%%%%%(b)%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2, 3, 2);
Op2D = zeros(m_length, n_length); % operational intensity
roofline2 = zeros(m_length, n_length);
bandwidth = 0.012;  % CPU-GPU bandwidth in Tflops
apeak = 6;  % arithmetic peak
% for test
b = 1024;
% row major data
for i = 1:m_length
    for j = 1:n_length
        Op2D(j,i) = (2*m(i)*n(j)*n(j))...
            ./ ((2*m(i)*n(j)*n(j)./b + m(i)*n(j))*8);
    end
end

roofline2 = min(Op2D * bandwidth, apeak);
surf(m, n, roofline2);

xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);
view(2); %% 2D view
colormap(jet(256)); hcb =colorbar; 
title(hcb,'Tflop/s', 'fontsize', fsize);
shading interp;
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$','(b) word\#: 2mnk/b + mn (b=1024)'},...
       'Interpreter', 'latex', 'fontsize', 20);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', 20);
zlabel('Tflop/s','fontsize', fsize);
axis square;
title('Roofline model (2) cublaXT', 'fontsize', fsize);

%%%%%%%%%%%%%%%%%%%%%%(c)%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2, 3, 3);
surf_data = csvread('gemm_perf.csv');
proposed = reshape(surf_data(:,13), [20,20]);
% surf(m, n, Tflops, 'FaceAlpha',0.8);
surf(m, n, proposed);
xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);
view(2); %% 2D view
colormap(jet(256));hcb =colorbar; 
title(hcb,'Tflop/s', 'fontsize', fsize);
shading interp;
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$','(b)'},...
       'Interpreter', 'latex', 'fontsize', 20);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', 20);
zlabel('Tflop/s','fontsize', fsize);
axis square;
title("Result of proposed method", 'fontsize', fsize);

%%%%%%%%%%%%%%%%%%%%%(d)%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,3,4);
cublasXT = reshape(surf_data(:,8), [20,20]);
% surf(m, n, Tflops, 'FaceAlpha',0.8);
surf(m, n, cublasXT);
xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);
view(2); %% 2D view
colormap(jet(256));hcb =colorbar;
title(hcb,'Tflop/s', 'fontsize', fsize);
shading interp;
set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(c)'},...
       'Interpreter', 'latex', 'fontsize', 20);
ylabel('n', 'fontsize', fsize);
zlabel('Tflop/s','fontsize', fsize);
axis square;
title("Result of cublasXT", 'fontsize', fsize);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%%%%%%%%%%%%%%(e)%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,3,5);
accle_rate = proposed ./ cublasXT;
% surf(m, n, Tflops, 'FaceAlpha',0.8);
surf(m, n, accle_rate);
xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
view(2); %% 2D view
colormap(jet(256));hcb =colorbar;
shading interp;
set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(d)'},...
       'Interpreter', 'latex', 'fontsize', 20);
ylabel('n', 'fontsize', fsize);
axis square;
title("Acceleration rate (proposed / cublasXT)", 'fontsize', fsize);
grid on; set(gca,'GridLineStyle',':');
