close all; clear;
fsize = 14;
h1=figure; set(h1, 'position', [200, 200, 800, 400]);
%%%%%%%%%%%%%%%%%%%%%%%%%%(a)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(1,2,1);

row_data = csvread('ts_breakdown.csv');
%row_data = csvread('row_breakdown.csv');

m = row_data(:,1); %% sample point
data_length = size(row_data,1);
percent_data = zeros(data_length, 5);
gemm_time  = row_data(:, 4);
qr_time    = row_data(:, 5);
svd_time   = row_data(:, 6);
total_time = row_data(:, 7);

percent_data(:, 1) = gemm_time./ total_time;
percent_data(:, 2) = qr_time ./ total_time;
percent_data(:, 3) = svd_time ./ total_time;
percent_data(:, 4) = 1.0 - percent_data(:, 1) - percent_data(:,2) - percent_data(:,3); 

stackh = area(m, percent_data .* 100.0);

stackh(1).FaceColor = [0 0.95 0.95];
stackh(2).FaceColor = [0.5 0 0.5];
stackh(3).FaceColor = [0.2 0.7 0];
stackh(4).FaceColor = [0.8 1 0];
stackh(5).FaceColor = [1 0 0];
set(gca,'Xscale', 'log');
set(stackh,'EdgeColor','None');
set(gca,'fontsize',14);
xlim([min(m) max(m)]);
ylim([0 100]);

h_legend = legend('GEMM',...
                  'QR',...
                  'SVD',...
                  'Misc.',...
                  'Orientation','Vertical',...
                  'Location','southwest');

set(h_legend,'FontSize',12);

axis square;
%title('Column sampling for square matrix (n:m:k=256:256:1)', 'fontsize', fsize);

xlabel({'$m$','(a) $n=5000$'},...
       'Interpreter', 'latex', 'fontsize', 20);

ylabel('Percentage %', 'fontsize', fsize);
%title('tall-skinny matrix');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%(b)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(1,2,2);

row_data = csvread('row_breakdown.csv');


m = row_data(:,1); %% sample point
n = row_data(:,2);
data_length = size(row_data,1);
percent_data = zeros(data_length, 5);
gemm_time  = row_data(:, 4);
qr_time    = row_data(:, 5);
svd_time   = row_data(:, 6);
total_time = row_data(:, 7);

percent_data(:, 1) = gemm_time./ total_time;
percent_data(:, 2) = qr_time ./ total_time;
percent_data(:, 3) = svd_time ./ total_time;
percent_data(:, 4) = 1.0 - percent_data(:, 1) - percent_data(:,2) - percent_data(:,3); 

stackh = area(n, percent_data .* 100.0);

stackh(1).FaceColor = [0 0.95 0.95];
stackh(2).FaceColor = [0.5 0 0.5];
stackh(3).FaceColor = [0.2 0.7 0];
stackh(4).FaceColor = [0.8 1 0];
stackh(5).FaceColor = [1 0 0];
set(gca,'Xscale', 'log');
set(stackh,'EdgeColor','None');
set(gca,'fontsize',14);
xlim([min(n) max(n)]);
ylim([0 100]);

h_legend = legend('GEMM',...
                  'QR',...
                  'SVD',...
                  'Misc.',...
                  'Orientation','Vertical',...
                  'Location','southwest');

set(h_legend,'FontSize',12);

axis square;
%title('Column sampling for square matrix (n:m:k=256:256:1)', 'fontsize', fsize);

xlabel({'$n$','(a) $m=10^5$'},...
       'Interpreter', 'latex', 'fontsize', 20);

ylabel('Percentage %', 'fontsize', fsize);


