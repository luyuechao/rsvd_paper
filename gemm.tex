
For many of the applications of our interests,
the matrix~$\mathbf{A}$ is tall-skinny (i.e., $\ell \ll n \ll m$). 
%
Hence,
the cost of the RSVD algorithm is dominated by
the matrix multiplication with $\mathbf{A}$
(on Lines 3, 5, and 8 in Algorithm~\ref{alg:rsvd}).
This is true on both the computational and data access costs
as well as the run time of the algorithm.
In this section, we examine the performance of 
this main computational kernel of RSVD,
GEMM, using multiple GPUs on the node.
%
Though our out-of-GPU-memory implementation of GEMM
has the same interface as the standard BLAS,
we use the notation which complies with the RSVD algorithm, i.e.,
$\mathbf{P} := \mathbf{AQ}$, where
$\mathbf{A}$, $\mathbf{Q}$, and $\mathbf{P}$ are the matrices
of dimensions $m \times n$, $n \times \ell$ and $m \times \ell$, respectively.



% ----------------------------------------------------------------- %
\subsection{Implementation}

Since $\ell \ll m,n$,
we assume that the projection basis vectors~$\mathbf{P}$ and $\mathbf{Q}$ fit in the GPU memory,
while we transfer only a part of the input matrix $\mathbf{A}$ from the
CPU to the GPU at a time.
%
As shown in \fref{fig:1DGEMM},
our implementation uses the 1D block row partition of the tall-skinny $\mathbf{A}$.
%whereas the 2D block partition is the standard for a general matrix.
The matrix $\mathbf{A}$ is partitioned into $s$ blocks,
and the dimension of each block is $b \times n$ (i.e., $m = s \cdot b$). 
%
We copy one block of $\mathbf{A}$ to the GPU and perform the corresponding GEMM using cuBLAS on the GPU at a time
(i.e., $\mathbf{P}(i) := \mathbf{A}(i) \mathbf{Q}$).
With multiple GPUs, these blocks are assigned to the GPUs in a round-robin fashion
(i.e., 1D block row cyclic distribution).
%
The $m \times \ell$ matrix $\mathbf{P}$ is partitioned
in the same 1D block row format as the matrix~$\mathbf{A}$,
while the small $n \times \ell$ matrix $\mathbf{Q}$ 
is initialized on the CPU, and then broadcasted to all the GPUs 
before the first step of RSVD.
%
%1D row partition can also be seen as a special case of 2D partition 
%when block size is set to be big enough to expand the number of column or row. 

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\hsize]{figs/partiton.pdf}
	\caption{1D block row cyclic partition of $\mathbf{A}$ for out-of-GPU-memory GEMM on three GPUs.
                 Blocks with the same color is copied to the same GPU.}
	\label{fig:1DGEMM}
\end{figure}

Though it is a bandwidth-limited operation,
the GEMM performance may be improved
by overlapping the useful computation 
with the transfer of the matrix blocks.
Hence, we use multiple GPU streams
to overlap the transfer of a block to the GPU
with the GEMM operation of another block on the GPU.
We execute the transfer and GEMM of different blocks
on different streams using the multiple streams in a round-robin
fashion.

Compared with the 1D block column or 2D block partition,
the main benefits of using the 1D block row partitions are:
(1)~Cache efficient: 
If we partition $\mathbf{A}$ in the 1D block column,
we perform the rank-$b$ outer-product update of the output matrix~$\mathbf{P}$
using each block column of $\mathbf{A}$.
The GPU's fast memory may not be large enough to hold the entire $\mathbf{P}$ at once.
Hence, the matrix $\mathbf{P}$ may need to be read into the fast memory for each block column.
In contrast, with the 1D block row partition,
each block GEMM performs all the updates to the corresponding block of $\mathbf{P}$.
Hence, $\mathbf{P}$ may need to be moved to the fast memory only once.
%
(2) Higher performance: cuBLAS GEMM is often optimized better for the square matrices
than for the tall-skinny matrices. Compared with the 1D block column partition,
the 1D block row partition leads to more square blocks, leading to higher performance of GEMM.
%
(3)~Simple implementation:
With the 1D block row partition,
each GEMM operation is independent from each other, and
it is easy to use multiple GPU streams
for executing the GEMM operations in parallel
(and hide the GEMM operations behind the transfer of blocks).
With the 1D block column partition,
each GEMM updates the whole matrix~$\mathbf{P}$.
Hence, we require multiple buffers and 
synchronization mechanism to combine the updates
from different streams.
%
(4)~Scalability: The 1D block column partition requires the reduction to combine the updates
from different streams or GPUs, which may limit the parallelism. 
The 1D block row partition is free of the reductions 
(independent GEMM operations, $\mathbf{P}(i)$ := $\mathbf{A}(i)\mathbf{Q}$, with different $i$
 can be performed in parallel)
which is essential for scalability with multiple GPUs or multiple GPU streams.

%However 1D row-partition also has its draw-backs. If the matrices are saved in column-major format, the copy stride (memory gap) grows with the increase of the height of $\mathbf{A}$. We will show the impact of the stride in section.
%ScaLAPACK, Slate use outer product GEMM. We used inner product GEMM so as to minimize communication. 


% ----------------------------------------------------------------- %
\subsection{Roofline Model}

Before showing the out-of-GPU-memory GEMM performance,
we use the roofline model~\cite{williams2009roofline}
to understand the performance.
The roofline model provides the performance upper bound defined as
\[
  \begin{array}{llll}
  roofline = \min( &\mbox{operational intensity} \times \mbox{peak bandwidth},\\
                   &\mbox{peak compute performance} \;\;\;), 
  \end{array}
\]
where the operational intensity is defined as 
\[
  \mbox{operational intensity} = \frac{\mbox{required flop count}}{\mbox{amount of required data transfer}}.
\]
In other words, the roofline performance is given by
\begin{eqnarray}
\nonumber
roofline & = & \min\left( \frac{flops}{D} \times BW,\ P \right)\\
\label{eq:roofline}
         & = & \min\left( \frac{flops}{D/BW},\ P \right),
\end{eqnarray}
where
``$P$'' is the peak computational performance of the hardware (e.g., Tflop/s), 
``flops'' is the number of required floating-point operations,
``$D$'' is  the amount of required data transfer, and
``$BW$'' is the hardware's peak bandwidth.
Hence,
``flops/D'' is the operational intensity and
``$D/BW$'' is the time required to transfer the data.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/roofline.pdf}
	\caption{Roofline Tflop/s model of tall-skinny out-of-GPU-memory DGEMM with NVIDIA Volta GPU: we used
        $13$ GB/s as the peak bandwidth $BW$, $7$ Tflop/s as the theoretical peak performance $P$, and $2mn\ell$ as the required flop count $flops$ 
        in \eqref{eq:roofline}.
%
        The 1D block row presents the ``best-case'' for the required amount of the data transfer, where all the matrices are copied between the
        CPU and GPU once, i.e., $mn+n\ell+m\ell$ and $mn$ for copying to and from the GPU, respectively.
        Since V100 is full-duplex, the transfer to and from the GPU can be fully overlapped, and hence we used $D= mn+n\ell+m\ell$.
%
        For 2D partition with square block of dimension $b \times b$, the ``worst-case'' represent the case where any data is never reused.
        This results in $D = (2n/b+1)m\ell+n\ell$.
%inversely proportional to the size of the partition tile $b$. 2D tiling inevitably brings the problem of data re-transfer which will quickly saturate the narrow CPU-GPU bandwidth. We refer this method to ``worst-case'' and show its performance upper bound in the roofline model.
}
	\label{fig:roofline}
\end{figure}
% =============================================================== %

To reveal the impacts of different shapes of the matrix on the performance of GEMM, 
the heatmap in \fref{fig:roofline} shows the roofline model for GEMM in double precision (DGEMM),
where the vertical and horizontal axes show the matrix dimensions $n$ and $m$ of $\mathbf{A}$, respectively.\footnote{%
We model the performance of the standard out-of-GPU-memory GEMM operation, and hence include 
the time needed to copy all the three input matrices~$\mathbf{A}$, $\mathbf{P}$,
and $\mathbf{Q}$ from the CPU to the GPU, and the time needed to copy the output matrix $\mathbf{P}$
back to the CPU.}
It illustrates that with the tall-skinny $\mathbf{A}$ (i.e., $m \gg n$),
the GEMM performance is communication-bound.
In addition, with the $n \times \ell$ matrix~$\mathbf{Q}$, 
the operational intensity is $\ell$ (i.e., performing $\ell$ flops for each matrix entry of $\mathbf{A}$ copied to the GPU).
Hence, the performance becomes bounded more by communication 
as the number of columns in $\mathbf{Q}$ decreases 
(i.e., a smaller value of $\ell$, and hence skinner $\mathbf{Q}$).
On the other hand, as the value of $\ell$ increases,
the GEMM performance may attain the computation peak performance 
when the data transfer is carefully implemented. 
%\fref{fig:roofline}(b), (d) shows that a 2D tiling strategy will degrade the performance ceilings to all bandwidth-bound 
%for out-of-GPU-memory GEMM. It illustrates that an efficient tiling and computation strategy are critical to performance.


%We use a data reuse rate $r$ to denotes the reuse time of a tile from $\mathbf{A}$ or $\mathbf{B}$.
%Our system is equipped with two Nvidia P100 GPU which is linked to two CPUs by two PCIe 3.0 links. 


% ----------------------------------------------------------------- %
\subsection{Effects of Block Size on Performance}

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/perf_tune.pdf}
	\caption{Effects of block size on the performance of out-of-GPU-memory DGEMM.}
	\label{fig:tune}
\end{figure}
% =============================================================== %

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/single_result.pdf}
	\caption{Out-of-GPU-memory DGEMM performance with one V100 GPU.}
	\label{fig:singleGEMM}
\end{figure}
% =============================================================== %


\fref{fig:tune} shows the performance of our out-of-GPU-memory DGEMM implementation,
using different block sizes, i.e., $b= 1024$, $2048$, $4096$, $8192$, and ``max block size''
denoting the maximum block size not exceeding the GPU memory. 
%
We first notice that the ``pageable memory" have the lowest performance, and hence, for the rest of the experiments,
we store all of the matrices $\mathbf{A}$, $\mathbf{Q}$ and $\mathbf{P}$ in the pinned memory. 
%
%When the matrix size grew larger than that GPU can hold, divide and conquer strategy is deployed which leads to a performance leap. 
%\fref{fig:tune} (a) and (b) shows a complete bandwidth-bound and a partially computation bound situation.
Our performance was lower than the roofline because for the tall-skinny matrices, we could not obtain
the peak bandwidth or the peak compute performance (i.e., we observed only $8 \sim 11$~MB/s and $8.3 \sim 9.3$ Gflop/s).
For a smaller matrix, a larger block size could lower the performance 
because 
the transfer of the first block cannot be overlapped with GEMM on the GPU.
In particular, for small matrices,
the ``maximum'' gave a poor performance. %when data transfer and GEMM are not properly overlapped.
However, for a large enough matrix, the effects of the block size on the performance
was not significant.
For the rest of the experiments, we use the block size of $b=4096$.
  

% ----------------------------------------------------------------- %
\subsection{Out-of-GPU-memory GEMM Performance Comparison}

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/two_gpus.pdf}
	\caption{Out-of-GPU-memory DGEMM performamce with two V100 GPUs.}
	\label{fig:twoGEMM}
\end{figure}
% =============================================================== %

We now compare the performance of our implementations with two other implementations 
cuBLAS-XT and BLASX. 
\fref{fig:singleGEMM} shows the DGEMM performance on one V100 GPU with four different matrix shapes. 
%
%We first experiment with different shapes in double precision. We used the default tile size in this microbenchmark. 
%
%
%the first thing we noticed is that the data transfer is overloaded. For any shapes of $\mathbf{A}$, $\mathbf{Q}$ and $\mathbf{P}$, cuBLAS-XT uses $1024$-$by$-$1024$ as its default tile size. 
%The shape of DGEMM kernels are all set to $[16,16,2]$ for grid and $[64, 1, 1]$ for thread blocks. %From the number of data transfer of $\mathbf{C}$ tile, we find the calculation of $\mathbf{C}$ tile is by inner product. 
%This methods leaves a very small memory footprint on GPU which severs particular usage while its performance is throttled by excessive data transfer.
Using NVIDIA profiler, 
we found that the default block size for cuBLAS-XT is $1024$.
%
The default block size performed well for the square matrices.
However,
for the tall-skinny matrices of our interests, 
cuBLAS-XT with the default block size
copied all the three blocks of $\mathbf{A}$, $\mathbf{P}$, and $\mathbf{Q}$
to the GPU for each GEMM operation without any data reuse.
Hence,
compared with our implementation, cuBLAS-XT obtained lower performance.
%
We also used the block size of $b=n$ 
that enforces the 1D block row partition and
obtained higher performance than the default 2D partition. 
%By counting the data transfer, we find there is no tile reuse in the computation. 
%
Next, we tried storing the matrices in the UMA memory instead of the pinned memory.
Using UMA gave a low performance even with the 1D partition. 
%
The performance of cuBLAS-XT with the default 2D partition
downgraded with the increase in the size of the matrix $\mathbf{Q}$ 
(see \fref{fig:singleGEMM}(a) and~(b)). 
%
The default performance of cuBLAS-XT was also lower than the other implementations 
for a short-wide matrix~$\mathbf{Q}$ (\fref{fig:singleGEMM}(d), \fref{fig:twoGEMM}(d)), 

\fref{fig:twoGEMM} shows the GEMM performance using two GPUs. We observed similar results
as in \fref{fig:singleGEMM}, except that most of the implementation scaled well
since the GPUs are connected to the CPU through separate PCIe.
%
\comment{
The multiplication with $\mathbf{A}^\top$ obtained 
the performance similar to that with $\mathbf{A}$. We omit further experiment details.}
%
For the rest of the experiments with cuBLAS-XT,
we used the 1D block row partition that obtained the best performance
for the tall-skinny GEMM operations.

%We come to the conclusion that 1D partition is suitable for tall-skinny GEMM.
%In the next section, we will further explore the benefits 1D GEMM creates in RSVD algorithm. 

%%Conclusion: duplication gives better performance
