
Algorithm~\ref{alg:rsvd} shows the RSVD algorithm.
The algorithm first generates the basis vectors~$\mathbf{Q}$ and 
$\mathbf{P}$ that approximate the range and domain of 
the matrix $\mathbf{A}$, respectively. 
To remove the noise and to improve the approximation,
it performs the power (or orthogonal) iteration~\cite{Golub:2012}
(Lines $2-7$ of Algorithm~\ref{alg:rsvd}). 
To maintain the numerical stability,
these basis vectors are orthogonalized during the iteration.
%
After the power iteration,
we compute the QR factorization of $\mathbf{P}$ on Line 9
such that the upper triangular matrix $\mathbf{B}$ is the projected matrix
of dimension $\ell \times \ell$
(i.e., $\mathbf{B} = \mathbf{P}^T \mathbf{A} \mathbf{Q}$).
We then compute the full SVD of $\mathbf{B}$.
Finally, to generate the approximation to the truncated SVD of $\mathbf{A}$ 
(i.e., $\mathbf{A} \approx \mathbf{U} \mathbf{\Sigma} \mathbf{V^{\top}}$),
the truncated SVD of $\mathbf{B}$ is projected back onto $\mathbf{Q}$ and $\mathbf{P}$ on Line 11. 
On return, $\mathbf{\Sigma}$ is the diagonal matrix whose diagonal entries approximate the $k$ largest
singular values of $\mathbf{A}$, while $\mathbf{U}$ and $\mathbf{V}$ approximate the corresponding
left and right singular vectors, respectively.
%Note that the orthonormalization of domain $\mathbf{P}$ can be omitted for shorter runtime.

\begin{algorithm}[t]
        \small
        \SetKwInOut{Input}{Input}
        \SetKwInOut{Output}{Output}
        %%      \Indm
        \Input{matrix $\mathbf{A} \in \mathbb{R}^{m \times n}$, target rank $k$, oversampling parameter $o$, and power iteration count $q$}
        \Output{$\mathbf{U}$, $\mathbf{\Sigma}$, and $\mathbf{V}$ such that $\mathbf{A} \approx \mathbf{U} \mathbf{\Sigma} \mathbf{V^{\top}}$
                with $k \times k$ diagonal $\mathbf{\Sigma}$, and orthonormal column vectors $\mathbf{U}$ and~$\mathbf{V}$.}

        %%      \Indp\Indpp
        \BlankLine
        Generate a random matrix $\mathbf{Q} \sim \mathcal{N}\left(0,1\right)^{n \times \ell}$,
        where $\ell = k + o$.

        \For{$i = 0, 1, \cdots, q-1$}{
                $\mathbf{P} := \mathbf{A}\mathbf{Q};$                   \hspace{3.4cm}$\mathcal{O}(mn\ell)$ flops

                $\mathbf{P} := orth\left(\mathbf{P}\right);$
                \ \/// if needed                                        \hspace{1.0cm}$\mathcal{O}(m\ell^2)$ flops

                $\mathbf{Q} := \mathbf{A}^{\top}\mathbf{P};$               \hspace{3.2cm}$\mathcal{O}(mn\ell)$ flops

                $\mathbf{Q} := orth(\mathbf{Q});$                       \hspace{2.8cm}$\mathcal{O}(n\ell^2)$ flops
        }
        $\mathbf{P} := \mathbf{A}\mathbf{Q};$

        $\left[ \mathbf{P},\ \mathbf{B}\right] := qr\left(\mathbf{P}\right);$  \hspace{2.9cm}$\mathcal{O}(mn\ell)$ flops

        $\left[\mathbf{\widetilde{U}},\ \mathbf{\widetilde{\Sigma}},\ \mathbf{\widetilde{V}}\right] := svd\left(\mathbf{B}\right);$
                                                                        \hspace{2.0cm}$\mathcal{O}(\ell^3)$ flops

        $\mathbf{\Sigma} := \mathbf{\widetilde{\Sigma}}(1:k,1:k);$

        $\mathbf{U} := \mathbf{P} \mathbf{\widetilde{U}}\left(:, 1:k\right);$ \hspace{2.7cm}$\mathcal{O}(m\ell k)$ flops

        $\mathbf{V} := \mathbf{Q}\mathbf{\widetilde{V}}\left(:,1:k\right);$   \hspace{2.7cm}$\mathcal{O}(n\ell k)$ flops

        \caption{Randomized SVD:
        $orth(\mathbf{P})$ orthogonalizes the column vectors $\mathbf{P}$, 
        while $qr(\mathbf{P})$ and $svd(\mathbf{B})$ return the QR factorization and SVD of matrices $\mathbf{P}$ and $\mathbf{B}$, 
        respectively. We use $\mathbf{\widetilde{\Sigma}}(1:k,1:k)$ and $\mathbf{\widetilde{U}}\left(:, 1:k\right)$ to denote
        the leading $k \times k$ submatrix of $\mathbf{\widetilde{\Sigma}}$ and the submatrix consiting of the first $k$ columns
        of $\mathbf{\widetilde{U}}$, respectively, while $\mathbf{A}^{\top}$ is the transpose of the matrix $\mathbf{A}$.
        }
        \label{alg:rsvd}
\end{algorithm}

It has been shown that for many of the matrices from the data science,
the randomized algorithm requires only a few accesses to $\mathbf{A}$
and outperformed the classical deterministic algorithm, while maintaining the desirable accuracy of the
computed approximation~\cite{halko2011finding,martinsson2006randomized, liberty2007randomized, martinsson2010normalized}.
